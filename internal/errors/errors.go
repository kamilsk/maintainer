package errors

import "go.octolab.org/errors"

const (
	Inconsistent errors.Message = "inconsistent"
)
