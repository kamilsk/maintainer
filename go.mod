module go.octolab.org/toolset/maintainer

go 1.15

require (
	github.com/alexeyco/simpletable v0.0.0-20200730140406-5bb24159ccfb
	github.com/golang/mock v1.4.4
	github.com/google/go-github/v33 v33.0.0
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/spf13/afero v1.5.1
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.7.0
	go.octolab.org v0.7.2
	go.octolab.org/toolkit/cli v0.4.1
	go.octolab.org/toolkit/config v0.0.4
	golang.org/x/oauth2 v0.0.0-20210113205817-d3ed898aa8a3
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	gopkg.in/yaml.v2 v2.4.0
)
